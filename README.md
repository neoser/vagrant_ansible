Provisioning
==============

Databases
==============
You can configure project databases:

	databases: 
	  - { name: ccesg,
	      download_backup: false,
	      connection:        
	        { host: uat.link,
	          user: phm,
	          password: Oth2_6n9, 
	          name: phm_ccesg_ccesg,
	          encoding: utf8,
	          port: 3306,
	          collation: utf8_general_ci }
	    }
where "name: ccesg" is name of database which will be created on Vagrant server
	"download_backup: false" defines abbility to download and backup database from external server. If you set it to true you need to set up connection parameters:
		connection:        
	        { host: uat.link,
	          user: phm,
	          password: Oth2_6n9, 
	          name: phm_ccesg_ccesg,
	          encoding: utf8,
	          port: 3306,
	          collation: utf8_general_ci }

	where host - external sever hostname (IP)
		user - external database user
		password - external database password
		name - external database name
		encoding - external database encoding
		port - external database server port
		collation - external database collation

If you need to create (download and back up) several databases you need to add several objects:
	
	databases: 
	  - { object 1 }
	  - { object 2 }

FTP Accees to Vagrant box
==============

    Hosthame: IP address defined in settings.yml
    Port: 22
    Username: vagrant
    Password: vagrant
    Protocol: SFTP

Also you can set up additional accounts in settings in settings.yml :
	
	sFTP:
	      additional_users: true
	      users:
	        - { name: 'username', 
	            password: 'userpassword', 
	            home_dir: 'path to home directory' }

where 'additional_users: true' means you want to create additional user. If you set it to false users object will be ignored (You can remove 'users' object there). If you need a couple of FTP users just create a couple users objects:
	
	users: 
	  - { object 1 }
	  - { object 2 }
